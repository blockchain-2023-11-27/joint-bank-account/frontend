# Joint Bank Account (No frameworks)

## Getting started

This app is coded in the most simple format, no frameworks are used in this case. It is written in plain HTML and javascript.

## Run and test

You need to install live server and run the application. Use the accounts returned by the backend and test the network. After adding the account to owners (public key), you should be able to see it by clicking "View Accounts".

You also need an digital wallet and connect to one account to be able to sign the transaction.

## Description

This is simple front end page to interact with the blockchain backend (joint bank account app).

## Usage

For demostration of blockchain backend of the join bank account application.
